package com.tanyray.event;

import com.tanyray.entity.Inventory;

public class InventoryUpdatedEvent extends InventoryEvent {
  private Inventory inventory;

  public InventoryUpdatedEvent() {
  }

  public InventoryUpdatedEvent(Inventory inventory) {
    this.inventory = inventory;
  }

  public Inventory getInventory() {
    return inventory;
  }

  public void setInventory(Inventory inventory) {
    this.inventory = inventory;
  }
}
