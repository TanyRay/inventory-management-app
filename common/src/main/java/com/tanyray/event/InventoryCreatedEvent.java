package com.tanyray.event;

import com.tanyray.entity.Inventory;

public class InventoryCreatedEvent extends InventoryEvent {
  private Inventory inventory;

  public InventoryCreatedEvent() {
  }

  public InventoryCreatedEvent(Inventory inventory) {
    this.inventory = inventory;
  }

  public Inventory getInventory() {
    return inventory;
  }

  public void setInventory(Inventory inventory) {
    this.inventory = inventory;
  }
}
