package com.tanyray.event;

import io.eventuate.Event;
import io.eventuate.EventEntity;

@EventEntity(entity = "com.tanyray.inventory.service.InventoryAggregate")
public abstract class InventoryEvent implements Event{

}
