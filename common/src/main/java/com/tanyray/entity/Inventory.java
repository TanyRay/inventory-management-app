package com.tanyray.entity;

public class Inventory {
  private Product product;
  private long count;

  public Inventory() {}

  public Inventory(Product product, long count) {
    this.product = product;
    this.count = count;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public long getCount() {
    return count;
  }

  public void setCount(long count) {
    this.count = count;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Inventory inventory = (Inventory) o;

    if (count != inventory.count) {
      return false;
    }
    return product != null ? product.equals(inventory.product) : inventory.product == null;
  }

  @Override
  public int hashCode() {
    int result = product != null ? product.hashCode() : 0;
    result = 31 * result + (int) (count ^ (count >>> 32));
    return result;
  }

  @Override
  public String toString() {
    String sb = "Inventory{" + "product=" + product +
        ", count=" + count +
        '}';
    return sb;
  }
}
