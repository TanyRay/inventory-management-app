package com.tanyray.entity;

import java.math.BigDecimal;


public class Product {
  private String id;
  private String name;
  private String sku;
  private BigDecimal price;
  private long expiredDate; // in millis

  public Product() {}

  public Product(String name, String sku, BigDecimal price, long expiredDate) {
    this.name = name;
    this.sku = sku;
    this.price = price;
    this.expiredDate = expiredDate;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public long getExpiredDate() {
    return expiredDate;
  }

  public void setExpiredDate(long expiredDate) {
    this.expiredDate = expiredDate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Product product = (Product) o;

    if (getExpiredDate() != product.getExpiredDate()) {
      return false;
    }
    if (getId() != null ? !getId().equals(product.getId()) : product.getId() != null) {
      return false;
    }
    if (getName() != null ? !getName().equals(product.getName()) : product.getName() != null) {
      return false;
    }
    if (getSku() != null ? !getSku().equals(product.getSku()) : product.getSku() != null) {
      return false;
    }
    return getPrice() != null ? getPrice().equals(product.getPrice()) : product.getPrice() == null;
  }

  @Override
  public int hashCode() {
    int result = getId() != null ? getId().hashCode() : 0;
    result = 31 * result + (getName() != null ? getName().hashCode() : 0);
    result = 31 * result + (getSku() != null ? getSku().hashCode() : 0);
    result = 31 * result + (getPrice() != null ? getPrice().hashCode() : 0);
    result = 31 * result + (int) (getExpiredDate() ^ (getExpiredDate() >>> 32));
    return result;
  }

  @Override
  public String toString() {
    String sb = "Product{" + "id='" + id + '\'' +
        ", name='" + name + '\'' +
        ", sku='" + sku + '\'' +
        ", price=" + price +
        ", expiredDate=" + expiredDate +
        '}';
    return sb;
  }
}

