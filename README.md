**Usage**

1) Evaluate docker host IP: `sh set-env.sh`
2) Setup Eventuate local environment: `docker-compose -f docker-compose-eventuate-local.yml up -d`
3) run app: `mvn -pl inventory-service,shop-service -am spring-boot:run`


**Stopping the application**
`docker-compose down -v`

**Short notes**
1) This task could be solved without thinking about consistency for several microservices 
(and it's probable what you expect) but I decided to look at the task and try Eventuate framework
http://eventuate.io/, promising consistency between 2 services with high probability

2) During this task I faced with a bunch of issues (thanks to my MacOS) with Docker, his previous setup via `brew`
and also Eventuate documentation inconsistent with any library version

3) For sure, it's incomplete implementation: and a lot of things to add: 
 * e2e-tests
 * unit-tests
 * logical improvements (currently, for simplicity, I imagine 1 Shop - 1 Storage with inventory items)
 * Lombok: to reduce boilerplate code
 
 PS. I'm going to add in a spare time or after my holiday
 
Anyway, thanks for an interesting task!
