package com.tanyray.shop.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.tanyray.entity.Inventory;
import com.tanyray.entity.Product;

import static org.springframework.data.mongodb.core.query.Criteria.where;

public class InventoryRepositoryImpl implements InventoryRepositoryCustom {

  private MongoTemplate mongoTemplate;

  @Autowired
  public InventoryRepositoryImpl(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @Override
  public void addInventory(String inventoryId, Inventory inventory) {
    mongoTemplate.save(inventory);
  }

  @Override
  public void deleteInventory(String inventoryId, Inventory inventory) {
    mongoTemplate.remove(new Query(where("_id").is(inventoryId)), Inventory.class);
  }

  @Override
  public void updateInventory(String inventoryId, Inventory inventory) {
    Query query = new Query(where("_id").is(inventoryId));

    DBObject dbDoc = new BasicDBObject();
    mongoTemplate.getConverter().write(inventory, dbDoc);
    dbDoc.removeField("_id");
    Update update = Update.fromDBObject(dbDoc);

    mongoTemplate.upsert(query, update, Inventory.class);
  }

  @Override
  public Product findInventoryBySku(String productSku) {
    Query query = new Query(where("product.sku").is(productSku).and("count").gt(0));
    Inventory inventory = mongoTemplate.findOne(query, Inventory.class);
    return inventory != null ? inventory.getProduct() : null;
  }

  @Override
  public Inventory findById(String inventoryId) {
    Query query = new Query(where("_id").is(inventoryId));
    return mongoTemplate.findOne(query, Inventory.class);
  }
}
