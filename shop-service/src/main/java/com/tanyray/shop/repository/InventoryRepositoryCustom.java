package com.tanyray.shop.repository;


import com.tanyray.entity.Inventory;
import com.tanyray.entity.Product;

public interface InventoryRepositoryCustom {

  void addInventory(final String id, final Inventory inventory);

  void deleteInventory(final String id, final Inventory inventory);

  void updateInventory(final String id, final Inventory inventory);

  Product findInventoryBySku(String productSku);

  Inventory findById(final String id);

}
