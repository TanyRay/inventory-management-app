package com.tanyray.shop.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories
public class InventoryMongoConfig {

  @Bean
  public InventoryRepositoryCustom inventoryUpdateService(MongoTemplate mongoTemplate) {
    return new InventoryRepositoryImpl(mongoTemplate);
  }

}
