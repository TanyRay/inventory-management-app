package com.tanyray.shop.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tanyray.entity.Inventory;

public interface InventoryRepository extends MongoRepository<Inventory, String>,
    InventoryRepositoryCustom {

}
