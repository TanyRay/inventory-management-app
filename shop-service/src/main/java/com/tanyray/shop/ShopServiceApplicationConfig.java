package com.tanyray.shop;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.tanyray.shop.repository.InventoryMongoConfig;
import com.tanyray.shop.repository.InventoryRepository;
import com.tanyray.shop.service.InventoryEventSubscriber;
import io.eventuate.javaclient.spring.EnableEventHandlers;

@Configuration
@ComponentScan
@Import({InventoryMongoConfig.class})
@EnableEventHandlers
public class ShopServiceApplicationConfig {

  @Bean
  public InventoryEventSubscriber inventoryHistoryWorkflow(InventoryRepository inventoryRepository) {
    return new InventoryEventSubscriber(inventoryRepository);
  }

}
