package com.tanyray.shop.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tanyray.entity.Product;
import com.tanyray.shop.repository.InventoryRepository;


@RestController
@RequestMapping("/shop")
public class ShopController {

  private final InventoryRepository repository;

  @Autowired
  public ShopController(InventoryRepository repository) {
    this.repository = repository;
  }


  @RequestMapping(path = "product/{sku}", method = RequestMethod.GET)
  public ResponseEntity<Product> getProductAvailability(@PathVariable("sku") String sku) {
    Product product = repository.findInventoryBySku(sku);
    return product != null ? ResponseEntity.ok(product) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

}
