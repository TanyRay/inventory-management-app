package com.tanyray.shop.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.tanyray.entity.Inventory;
import com.tanyray.event.InventoryDeletedEvent;
import com.tanyray.event.InventoryUpdatedEvent;
import com.tanyray.shop.repository.InventoryRepository;
import io.eventuate.DispatchedEvent;
import io.eventuate.EventHandlerMethod;
import io.eventuate.EventSubscriber;
import com.tanyray.event.InventoryCreatedEvent;

@EventSubscriber(id = "inventorySubscriber")
public class InventoryEventSubscriber {

  private InventoryRepository inventoryRepository;

  @Autowired
  public InventoryEventSubscriber(InventoryRepository inventoryRepository) {
    this.inventoryRepository = inventoryRepository;
  }

  @EventHandlerMethod
  public void create(DispatchedEvent<InventoryCreatedEvent> dispatchedEvent) {
    InventoryCreatedEvent event = dispatchedEvent.getEvent();
    String inventoryId = dispatchedEvent.getEntityId();
    this.inventoryRepository.addInventory(inventoryId, event.getInventory());
  }

  @EventHandlerMethod
  public void delete(DispatchedEvent<InventoryDeletedEvent> dispatchedEvent) {
    String inventoryId = dispatchedEvent.getEntityId();
    Inventory inventory = this.inventoryRepository.findById(inventoryId);
    this.inventoryRepository.deleteInventory(inventoryId, inventory);
  }

  @EventHandlerMethod
  public void update(DispatchedEvent<InventoryUpdatedEvent> dispatchedEvent) {
    InventoryUpdatedEvent event = dispatchedEvent.getEvent();
    String inventoryId = dispatchedEvent.getEntityId();
    Inventory inventory = this.inventoryRepository.findById(inventoryId);
    this.inventoryRepository.deleteInventory(inventoryId, inventory);
    this.inventoryRepository.addInventory(inventoryId, event.getInventory());
  }

}
