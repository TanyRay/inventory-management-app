package com.tanyray.inventory.service;

import java.util.concurrent.CompletableFuture;

import com.tanyray.inventory.command.CreateInventoryCommand;
import com.tanyray.inventory.command.DeleteInventoryCommand;
import com.tanyray.inventory.command.InventoryCommand;
import com.tanyray.inventory.command.UpdateInventoryCommand;
import com.tanyray.entity.Inventory;
import io.eventuate.AggregateRepository;
import io.eventuate.EntityWithIdAndVersion;


public class InventoryService {

  private final AggregateRepository<InventoryAggregate, InventoryCommand> repository;

  public InventoryService(AggregateRepository<InventoryAggregate, InventoryCommand> repository) {
    this.repository = repository;
  }

  public CompletableFuture<EntityWithIdAndVersion<InventoryAggregate>> save(Inventory inventory) {
    return repository.save(new CreateInventoryCommand(inventory));
  }

  public CompletableFuture<EntityWithIdAndVersion<InventoryAggregate>> update(String inventoryId,
      Inventory inventory) {
    return repository.update(inventoryId, new UpdateInventoryCommand(inventory));
  }

  public CompletableFuture<EntityWithIdAndVersion<InventoryAggregate>> delete(String inventoryId) {
    return repository.update(inventoryId, new DeleteInventoryCommand());
  }
}
