package com.tanyray.inventory.service;

import java.util.ArrayList;
import java.util.List;

import com.tanyray.inventory.command.CreateInventoryCommand;
import com.tanyray.inventory.command.DeleteInventoryCommand;
import com.tanyray.inventory.command.InventoryCommand;
import com.tanyray.inventory.command.UpdateInventoryCommand;
import com.tanyray.entity.Inventory;
import com.tanyray.event.InventoryCreatedEvent;
import com.tanyray.event.InventoryDeletedEvent;
import com.tanyray.event.InventoryUpdatedEvent;
import io.eventuate.Event;
import io.eventuate.EventUtil;
import io.eventuate.ReflectiveMutableCommandProcessingAggregate;

/**
 * Command to event translator based on combinations of event states
 */
public class InventoryAggregate extends ReflectiveMutableCommandProcessingAggregate<InventoryAggregate, InventoryCommand> {
  private Inventory inventory;
  private boolean deleted = false;

  public List<Event> process(CreateInventoryCommand cmd) {
    return EventUtil.events(new InventoryCreatedEvent(cmd.getInventory()));
  }

  public List<Event> process(UpdateInventoryCommand cmd) {
    if (!this.deleted) {
      return EventUtil.events(new InventoryUpdatedEvent(cmd.getInventory()));
    }
    return new ArrayList<>();
  }

  public List<Event> process(DeleteInventoryCommand cmd) {
    if (!this.deleted) {
      return EventUtil.events(new InventoryDeletedEvent());
    }
    return new ArrayList<>();
  }


  public void apply(InventoryCreatedEvent event) {
    this.inventory = event.getInventory();
  }

  public void apply(InventoryUpdatedEvent event) {
    this.inventory = event.getInventory();
  }

  public void apply(InventoryDeletedEvent event) {
    this.deleted = true;
  }

  public Inventory getInventory() {
    return inventory;
  }
}
