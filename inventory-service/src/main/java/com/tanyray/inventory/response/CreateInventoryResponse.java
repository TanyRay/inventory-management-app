package com.tanyray.inventory.response;

public class CreateInventoryResponse {
  private String inventoryId;

  private CreateInventoryResponse() {}

  public CreateInventoryResponse(String inventoryId) {
    this.inventoryId = inventoryId;
  }

  public String getInventoryId() {
    return inventoryId;
  }

  public void setInventoryId(String inventoryId) {
    this.inventoryId = inventoryId;
  }
}
