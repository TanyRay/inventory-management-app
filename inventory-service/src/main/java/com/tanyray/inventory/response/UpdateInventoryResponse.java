package com.tanyray.inventory.response;

public class UpdateInventoryResponse {
  private String version;

  public UpdateInventoryResponse() {
  }

  public UpdateInventoryResponse(String version) {
    this.version = version;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }
}
