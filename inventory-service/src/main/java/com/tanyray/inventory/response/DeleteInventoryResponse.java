package com.tanyray.inventory.response;

public class DeleteInventoryResponse {
  private String version;

  public DeleteInventoryResponse() {
  }

  public DeleteInventoryResponse(String version) {
    this.version = version;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }
}
