package com.tanyray.inventory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.tanyray.inventory.command.InventoryCommand;
import com.tanyray.inventory.service.InventoryAggregate;
import com.tanyray.inventory.service.InventoryService;
import io.eventuate.AggregateRepository;
import io.eventuate.EventuateAggregateStore;
import io.eventuate.javaclient.commonimpl.AggregateCrud;


@Configuration
@ComponentScan
public class InventoryServiceConfig {
  @Bean
  public AggregateRepository<InventoryAggregate, InventoryCommand> aggregateRepository(EventuateAggregateStore aggregateStore) {
    return new AggregateRepository<>(InventoryAggregate.class, aggregateStore);
  }

  @Bean
  public InventoryService inventoryService(AggregateRepository<InventoryAggregate, InventoryCommand> aggregateRepository) {
    return new InventoryService(aggregateRepository);
  }

}
