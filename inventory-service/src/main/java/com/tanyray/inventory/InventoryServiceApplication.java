package com.tanyray.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.tanyray.inventory.web.InventoryServiceWebConfig;
import io.eventuate.javaclient.driver.EventuateDriverConfiguration;


@SpringBootApplication
@Import({InventoryServiceWebConfig.class, EventuateDriverConfiguration.class})
public class InventoryServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(InventoryServiceApplication.class, args);
  }
}
