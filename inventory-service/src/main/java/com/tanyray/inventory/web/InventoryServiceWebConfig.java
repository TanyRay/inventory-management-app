package com.tanyray.inventory.web;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.tanyray.inventory.InventoryServiceConfig;
import com.tanyray.web.WebConfig;

@Configuration
@ComponentScan
@Import({InventoryServiceConfig.class, WebConfig.class})
public class InventoryServiceWebConfig {

}
