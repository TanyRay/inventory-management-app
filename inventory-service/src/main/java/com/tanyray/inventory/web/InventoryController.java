package com.tanyray.inventory.web;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tanyray.entity.Inventory;
import com.tanyray.inventory.response.CreateInventoryResponse;
import com.tanyray.inventory.response.DeleteInventoryResponse;
import com.tanyray.inventory.response.UpdateInventoryResponse;
import com.tanyray.inventory.service.InventoryService;

@RestController
@RequestMapping("/inventory")
public class InventoryController {

  private InventoryService inventoryService;

  @Autowired
  public InventoryController(InventoryService inventoryService) {
    this.inventoryService = inventoryService;
  }

  @RequestMapping(method = RequestMethod.POST)
  public CompletableFuture<CreateInventoryResponse> createInventory(
      @RequestBody Inventory request) {
    return inventoryService.save(request).thenApply(
        entityAndEventInfo -> new CreateInventoryResponse(entityAndEventInfo.getEntityId()));
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public CompletableFuture<UpdateInventoryResponse> updateInventory(
      @PathVariable("id") String inventoryId, @RequestBody Inventory request) {
    return inventoryService.update(inventoryId, request).thenApply(
        entityAndEventInfo -> new UpdateInventoryResponse(
            entityAndEventInfo.getEntityVersion().asString()));
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public CompletableFuture<DeleteInventoryResponse> deleteInventory(
      @PathVariable("id") String inventoryId) {
    return inventoryService.delete(inventoryId).thenApply(
        entityAndEventInfo -> new DeleteInventoryResponse(
            entityAndEventInfo.getEntityVersion().asString()));
  }

}
