package com.tanyray.inventory.command;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.tanyray.entity.Inventory;

public class CreateInventoryCommand extends InventoryCommand {

  private Inventory inventory;

  public CreateInventoryCommand() {
  }

  public CreateInventoryCommand(Inventory inventory) {
    this.inventory = inventory;
  }

  public Inventory getInventory() {
    return inventory;
  }

  public void setInventory(Inventory inventory) {
    this.inventory = inventory;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("inventory", inventory)
        .toString();
  }
}
