package com.tanyray.inventory.command;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.tanyray.entity.Inventory;

public class UpdateInventoryCommand extends InventoryCommand {
  private Inventory inventory;

  public UpdateInventoryCommand() {}

  public UpdateInventoryCommand(Inventory inventory) {
    this.inventory = inventory;
  }

  public Inventory getInventory() {
    return inventory;
  }

  public void setInventory(Inventory inventory) {
    this.inventory = inventory;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("inventory", inventory)
        .toString();
  }
}
