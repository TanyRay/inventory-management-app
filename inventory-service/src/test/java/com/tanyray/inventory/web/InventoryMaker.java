package com.tanyray.inventory.web;


import java.math.BigDecimal;

import org.joda.time.DateTime;

import com.tanyray.entity.Inventory;
import com.tanyray.entity.Product;

public class InventoryMaker {

  public static Inventory make() {
    return make(1, DateTime.now().plusYears(10).getMillis());
  }

  public static Inventory make(int count, long timestamp) {
    Product product = makeProduct(timestamp);
    return new Inventory(product, count);
  }

  private static Product makeProduct(long expiryDate) {
    return new Product("Deuter AIRCONTACT PRO 55 + 15 SL", "3330017-3033", new BigDecimal(50),
                       expiryDate);
  }

}
