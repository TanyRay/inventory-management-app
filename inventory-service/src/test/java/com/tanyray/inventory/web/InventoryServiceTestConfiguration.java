package com.tanyray.inventory.web;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import io.eventuate.javaclient.spring.jdbc.EmbeddedTestAggregateStoreConfiguration;


@Configuration
@Import({InventoryServiceWebConfig.class, EmbeddedTestAggregateStoreConfiguration.class})
@ComponentScan
public class InventoryServiceTestConfiguration {

  @Bean
  public RestTemplate restTemplate() {
    RestTemplate restTemplate = new RestTemplate();
    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    List<? extends HttpMessageConverter<?>> httpMessageConverters = Collections
        .singletonList(converter);
    restTemplate.setMessageConverters((List<HttpMessageConverter<?>>) httpMessageConverters);
    return  restTemplate;
  }

}
