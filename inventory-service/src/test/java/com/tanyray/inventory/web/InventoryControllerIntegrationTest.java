package com.tanyray.inventory.web;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanyray.inventory.response.CreateInventoryResponse;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * TODO find explanation, why InventoryServiceApplication.EventuateDriverConfiguration creates
 * qualifier issue for InventoryServiceTestConfiguration.EmbeddedTestAggregateStoreConfiguration
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {InventoryServiceTestConfiguration.class})
public class InventoryControllerIntegrationTest {

  public static final String INVENTORY_ROUTE = "/inventory";

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  @Before
  public void setUp() throws Exception {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @Test
  public void shouldCreateAndUpdateInventory() throws Exception {
    ObjectMapper objectMapper = new ObjectMapper();
    String content = objectMapper.writeValueAsString(InventoryMaker.make());

    MvcResult mvcResult = mockMvc.perform(post(INVENTORY_ROUTE).contentType(
        MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(content))
                                 .andReturn();

    mvcResult.getAsyncResult();

    mockMvc.perform(MockMvcRequestBuilders.asyncDispatch(mvcResult)).andExpect(status().isOk())
           .andDo(print()).andReturn();

    CreateInventoryResponse createInventoryResponse = (CreateInventoryResponse) mvcResult
        .getAsyncResult();

    String inventoryId = createInventoryResponse.getInventoryId();

    mockMvc.perform(put(INVENTORY_ROUTE + "/" + inventoryId).contentType(MediaType.APPLICATION_JSON)
                                                            .accept(MediaType.APPLICATION_JSON)
                                                            .content(objectMapper
                                                                         .writeValueAsString(
                                                                             InventoryMaker.make(2,
                                                                                                 DateTime
                                                                                                     .now()
                                                                                                     .getMillis()))))
           .andExpect(status().isOk());
  }
}
